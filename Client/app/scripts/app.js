'use strict'
import angular from 'angular'
import ngRoute from 'angular-route'
import ngResource from 'angular-resource'
import ngAnimate from 'angular-animate'
import uiBootstrap from 'angular-ui-bootstrap'
import ngLoadingBar from 'angular-loading-bar'
import fkControllers from './app.controllers'
import fkServices from './app.services'
import fkDirectives from './app.directives'
import fkFilters from './app.filters'
import {paths, auth} from './app.routes'
import config from './app.config'

angular.module('folks', [
  ngRoute,
  ngResource,
  ngAnimate,
  ngLoadingBar,
  uiBootstrap,
  fkControllers,
  fkServices,
  fkDirectives,
  fkFilters
])
.config(config)
.config(paths)
.run(auth)
