'use strict'
import angular from 'angular'

class ChatController {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
    this.connect()
  }

  init () {
    this.messages = []
    this.text = ''
    this.tags = []
    this.regex = /((?:|^\s)(?:#)([a-zA-Zà-úÀ-Ú\d]+))/gm
    this.tagIndex = -1
    this.tag = '#todas'
  }

  connect () {
    if (this.ChatService.connect(this.group.id)) {
      this.ChatService.subscribe('onclose', (event) => {
        const msg = 'Você foi desconectado.'
        this.ToastService.now(msg)
        this.ChatService.close()
      })
      this.ChatService.subscribe('onerror', (event) => {
        const msg = `Você está desconectado, por favor recarregue. Erro: ${event.data}`
        this.ToastService.now(msg)
        this.ChatService.close()
      })
      this.ChatService.subscribe('onmessage', (event) => {
        let {data} = angular.fromJson(event.data)
        if (data.tags.indexOf(this.tag) >= 0) {
          this.tags = Array.from(new Set([...this.tags, ...data.tags]))
          this.messages = [...this.messages, data]
        }
        this.cfpLoadingBar.complete()
      })
      this.cfpLoadingBar.start()
    } else {
      const msg = 'Conexão não estabelecida, por favor recarregue.'
      this.ToastService.now(msg)
    }
  }

  send () {
    let matches = new Set()
    let match
    while ((match = this.regex.exec(this.text))) {
      matches.add(match[1])
    }
    let msg = {
      user: this.user.id,
      group: this.group.id,
      message: this.text,
      tags: Array.from(matches)
    }
    this.text = ''
    this.ChatService.send('$message', msg)
  }

  selectTag (tag, index) {
    if (index !== this.tagIndex) {
      this.tagIndex = index
      this.tag = tag
      this.ChatService.send('$load', {
        field: 'tags',
        data: tag
      })
      this.messages = []
      this.cfpLoadingBar.start()
    }
  }
}

ChatController.$inject = [
  'user',
  'group',
  'cfpLoadingBar',
  '$location',
  '$scope',
  'ChatService',
  'ToastService'
]
export default angular.module(
  'folks.controllers.chatcontroller', []
).controller('ChatController', ChatController).name
