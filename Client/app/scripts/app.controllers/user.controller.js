'use strict'
import angular from 'angular'

class UserController {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.tooltips = {
      searchfield: 'Aqui você pode procurar por novos grupos.',
      searchicon: 'Clique aqui para procurar novos grupos.',
      newgroup: 'Clique aqui para criar um novo grupo.',
      groupinfo: 'Clique para mais informações sobre o grupo.'
    }
    this.groupsfound = []
  }

  getGroups (groupname = '') {
    if (groupname.length) {
      this.groupsfound = this.GroupService.getGroups({
        field: 'groupname',
        data: groupname
      })
    }
  }

  clear (field) {
    if (field.length === 0) {
      this.groupsfound = []
    }
  }
}

UserController.$inject = ['user', 'groups', 'GroupService']
export default angular.module(
  'folks.controllers.usercontroller', []
).controller('UserController', UserController).name
