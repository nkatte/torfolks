'use strict'
import angular from 'angular'
import HomeController from './home.controller'
import UserController from './user.controller'
import GroupCreateController from './group-create.controller'
import GroupDetailController from './group-detail.controller'
import ChatController from './chat.controller'

export default angular.module('folks.controllers', [
  HomeController,
  UserController,
  GroupCreateController,
  GroupDetailController,
  ChatController
]).name
