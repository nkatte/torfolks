'use strict'
import angular from 'angular'

class GroupDetailController {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.tooltips = {
      searchfield: 'Utilize o campo para encontrar os usuários desejados.'
    }
  }

  insertParticipant (user) {
    this.group.participants.push(user)
    this.group.applicants = this.group.applicants.filter(
      (item) => { return item.id !== user.id }
    )
    this.group.$save()
      .then((res) => {
        const msg = 'Usuário inserido com sucesso.'
        this.ToastService.now({
          message: msg,
          type: 'success'
        })
      }, (error) => {
        this.group.applicants.push(user)
        this.group.participants = this.group.participants.filter(
          (item) => { return item.id !== user.id }
        )
        const msg = `Ocorreu um erro. ${error.data.reason}`
        this.ToastService.now(msg)
      }
    )
  }

  insertApplicant (user) {
    this.group.applicants.push(user)
    this.group.$save()
      .then((res) => {
        const msg = 'Solicitação encaminhada.'
        this.ToastService.now({
          message: msg,
          type: 'success'
        })
      }, (error) => {
        this.group.applicants = this.group.applicants.filter(
          (item) => { return item.id !== user.id }
        )
        const msg = `Ocorreu um erro. ${error.data.reason}`
        this.ToastService.now(msg)
      }
    )
  }

  isCreator () {
    return this.group.creator.id === this.user.id
  }

  isApplicant () {
    return this.group.applicants.findIndex(
      (item) => { return item.id === this.user.id }
    ) >= 0
  }

  isParticipant () {
    return this.group.participants.findIndex(
      (item) => { return item.id === this.user.id }
    ) >= 0
  }
}

GroupDetailController.$inject = [
  'user',
  'group',
  'ToastService'
]
export default angular.module(
  'folks.controllers.groupdetailcontroller', []
).controller('GroupDetailController', GroupDetailController).name
