'use strict'
import angular from 'angular'

class GroupCreateController {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.groupname = ''
    this.description = ''
    this.user = this.AuthService.getUser()
    this.participants = [this.user]
    this.applicants = []
  }

  createGroup () {
    this.GroupService.createGroup(
      this.groupname,
      this.description,
      this.user,
      this.participants,
      this.applicants
    ).then((response) => {
      this.ToastService.next({
        message: 'Grupo criado com sucesso.',
        type: 'success'
      })
      let path = `/group/${response.id}`
      this.$location.path(path).replace()
    }, (error) => {
      const msg = `Ocorreu um erro ao criar o grupo. ${error.data.reason}`
      this.ToastService.now(msg)
    })
  }
}

GroupCreateController.$inject = [
  '$location',
  'AuthService',
  'GroupService',
  'ToastService'
]
export default angular.module(
  'folks.controllers.groupcreatecontroller', []
).controller('GroupCreateController', GroupCreateController).name
