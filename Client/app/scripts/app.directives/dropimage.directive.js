'use strict'
import angular from 'angular'

const DropImage = ($window) => ({
  restrict: 'A',
  scope: {
    fkDropImageModel: '='
  },
  link (scope, element, attrs) {
    const canvas = element[0]
    const ctx = canvas.getContext('2d')
    ctx.fillStyle = '#dddddd'
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    canvas.addEventListener('dragover', (e) => { e.preventDefault() }, true)
    canvas.addEventListener('drop', (e) => {
      e.preventDefault()
      const src = e.dataTransfer.files[0]
      if (!src.type.match(/image.*/)) {
        $window.alert('O arquivo não é uma imagem: ', src.type)
      } else {
        const reader = new $window.FileReader()
        reader.onload = (e) => {
          const image = new $window.Image()
          image.onload = function () {
            ctx.clearRect(0, 0, canvas.width, canvas.height)
            ctx.drawImage(image, 0, 0, canvas.width, canvas.height)
            scope.fkDropImageModel = canvas.toDataURL('image/png')
          }
          image.src = e.target.result
        }
        reader.readAsDataURL(src)
      }
    }, true)
  }
})

DropImage.$inject = ['$window']
export default angular.module(
  'folks.directives.dropimage', []
).directive('fkDropImage', DropImage).name
