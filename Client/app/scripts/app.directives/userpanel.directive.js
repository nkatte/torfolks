'use strict'
import angular from 'angular'

const UserPanelDirective = () => ({
  restrict: 'A',
  scope: {
    user: '<fkUserPanelModel'
  },
  template: `
  <div class="panel panel-default">
    <div class="panel-body">
      <img ng-src="{{::user.image}}" alt="User photo" class="center-block img-responsive" width="128" height="128" />
    </div>
    <div class="panel-footer text-center">
      <a href="/user">{{::user.username}}</a>
    </div>
  </div>
  `
})

export default angular.module(
  'folks.directives.userpaneldirective', []
).directive('fkUserPanel', UserPanelDirective).name
