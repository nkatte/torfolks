'use strict'
import angular from 'angular'
import Toast from './toast.directive'
import DropImage from './dropimage.directive'
import UserPanel from './userpanel.directive'
import ParticipantsPanel from './participantspanel.directive'

export default angular.module('folks.directives', [
  Toast,
  DropImage,
  UserPanel,
  ParticipantsPanel
]).name
