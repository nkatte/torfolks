'use strict'
import angular from 'angular'

const ParticipantsPanelDirective = () => ({
  restrict: 'A',
  scope: {
    participants: '<fkParticipantsPanelModel'
  },
  template: `
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4>Participantes</h4>
    </div>
    <div class="panel-body scrollable-list">
      <ul class="media-list">
        <li ng-repeat="user in participants" class="media">
          <div class="media-left media-middle">
            <img class="media-object" ng-src="{{::user.image}}" alt="User photo" width="64" height="64" />
          </div>
          <div class="media-body">
            <h4>{{::user.fullname}}<small> - {{::user.username}}</small></h4>
          </div>
        </li>
      </ul>
    </div>
  </div>
  `
})

export default angular.module(
  'folks.directives.participantspaneldirective', []
).directive('fkParticipantsPanel', ParticipantsPanelDirective).name
