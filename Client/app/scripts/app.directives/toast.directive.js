'use strict'
import angular from 'angular'

const ToastDirective = (ToastService) => ({
  restrict: 'E',
  scope: true,
  controllerAs: 'vm',
  controller: function () {
    this.ToastService = ToastService
  },
  template: '<uib-alert dismiss-on-timeout="5000" ng-repeat="msg in vm.ToastService.messages() track by $index" close="vm.ToastService.close($index)" type="{{::msg.type}}">{{::msg.message}}</uib-alert>'
})

ToastDirective.$inject = ['ToastService']
export default angular.module(
  'folks.directives.toastdirective', []
).directive('fkToast', ToastDirective).name
