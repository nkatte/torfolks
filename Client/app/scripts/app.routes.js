'use strict'
paths.$inject = ['$routeProvider', '$locationProvider']
function paths ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/user', {
      templateUrl: 'static/partials/user.view.html',
      controller: 'UserController',
      controllerAs: 'vm',
      resolve: {
        user: [
          'AuthService',
          (AuthService) => {
            return AuthService.getUser()
          }
        ],
        groups: [
          'AuthService',
          'GroupService',
          (AuthService, GroupService) => {
            const user = AuthService.getUser()
            return GroupService.getGroups({
              field: 'participants',
              data: {'$oid': user.id}
            }).$promise
          }
        ]
      },
      private: true
    })
    .when('/group', {
      templateUrl: 'static/partials/group-create.view.html',
      controller: 'GroupCreateController',
      controllerAs: 'vm',
      private: true
    })
    .when('/group/:groupId', {
      templateUrl: 'static/partials/group-detail.view.html',
      controller: 'GroupDetailController',
      controllerAs: 'vm',
      resolve: {
        user: [
          'AuthService',
          (AuthService) => {
            return AuthService.getUser()
          }
        ],
        group: [
          '$route',
          'GroupService',
          ($route, GroupService) => {
            return GroupService.getGroup(
              $route.current.params.groupId
            ).$promise
          }
        ]
      },
      private: true
    })
    .when('/chat/:groupId', {
      templateUrl: 'static/partials/chat.view.html',
      controller: 'ChatController',
      controllerAs: 'vm',
      resolve: {
        user: [
          'AuthService',
          (AuthService) => {
            return AuthService.getUser()
          }
        ],
        group: [
          '$route',
          'GroupService',
          ($route, GroupService) => {
            return GroupService.getGroup(
              $route.current.params.groupId
            ).$promise
          }
        ]
      },
      private: true
    })
    .otherwise({
      redirectTo: '/'
    })
  $locationProvider.html5Mode(true)
}

auth.$inject = ['$rootScope', '$location', 'AuthService']
function auth ($rootScope, $location, AuthService) {
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if (next.private) {
      if (!AuthService.isLoggedIn()) {
        $location.path('/')
      }
    } else {
      if (AuthService.isLoggedIn()) {
        $location.path('/user')
      }
    }
  })
}

export {paths, auth}
