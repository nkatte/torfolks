'use strict'
import angular from 'angular'

fallback.$inject = ['filterFilter']
function fallback (filterFilter) {
  function fallbackFilter (source, criteria, fallback) {
    let out = filterFilter(source, criteria)
    if (out.length === 0) {
      out = filterFilter(fallback, criteria)
    }
    return out
  }
  return fallbackFilter
}

export default angular.module(
  'folks.filters', []
)
.filter('fallback', fallback)
.name
