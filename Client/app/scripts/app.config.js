'use strict'
function config ($httpProvider) {
  $httpProvider.defaults.withCredentials = true
  $httpProvider.defaults.xsrfCookieName = '_xsrf'
  $httpProvider.defaults.xsrfHeaderName = 'X-XSRFToken'
  $httpProvider.defaults.headers.common = {'Accept': 'application/json'}
  $httpProvider.defaults.headers.post = {'Content-Type': 'application/json'}
}

config.$inject = ['$httpProvider']
export default config
