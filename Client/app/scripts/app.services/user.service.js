'use strict'
import angular from 'angular'

class UserService {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }
  init () {
    this.User = this.$resource(
      'api/user/:userId',
      {userId: '@userId'},
      null,
      {stripTrailingSlashes: false}
    )
  }

  getUser (userId) {
    return this.User.get({userId: userId})
  }

  getUsers (criteria) {
    return this.User.query({criteria: criteria})
  }

  createUser (username, fullname, password, image) {
    return new this.User({
      username: username,
      password: password,
      fullname: fullname,
      image: image
    }).$save()
  }
}

UserService.$inject = ['$resource']
export default angular.module(
  'folks.services.userservice', []
).service('UserService', UserService).name
