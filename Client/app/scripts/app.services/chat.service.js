'use strict'
import angular from 'angular'

class ChatService {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.ws = null
  }

  subscribe (name, callback) {
    const names = ['onopen', 'onmessage', 'onclose', 'onerror']
    function contains (arr, fun) {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] === fun) {
          return true
        }
      }
      return false
    }
    if (this.ws) {
      if (contains(names, name)) {
        this.ws[name] = callback
      }
    }
  }

  connect (chatId) {
    if (chatId) {
      const host = this.$location.host()
      const port = '8888'
      const url = `ws://${host}:${port}/api/chat/${chatId}`
      this.ws = new this.$window.WebSocket(url)
      if (this.ws.readyState === 0 || this.ws.readyState === 1) {
        return true
      }
    }
    return false
  }

  send (event, data) {
    let message = {
      event: event,
      data: data
    }
    this.ws.send(angular.toJson(message))
  }

  close () {
    this.ws.close()
    this.ws = null
  }
}

ChatService.$inject = ['$location', '$window']
export default angular.module(
  'folks.services.chatservice', []
).service('ChatService', ChatService).name
