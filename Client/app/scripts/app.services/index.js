'use strict'
import angular from 'angular'
import AuthService from './auth.service'
import UserService from './user.service'
import GroupService from './group.service'
import ChatService from './chat.service'
import ToastService from './toast.service'

export default angular.module('folks.services', [
  AuthService,
  UserService,
  GroupService,
  ChatService,
  ToastService
]).name
