'use strict'
import angular from 'angular'

class AuthService {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.user = null
  }

  isLoggedIn () {
    if (this.user) {
      return true
    } else {
      return false
    }
  }

  getUser () {
    return this.user
  }

  login (username, password) {
    return this.$q((resolve, reject) => {
      this.$http.post('/api/login', {
        username: username,
        password: password
      }).then((result) => {
        this.user = result.data.user
        resolve(result)
      }, (error) => {
        reject(error)
      })
    })
  }

  logout () {
    return this.$q((resolve, reject) => {
      this.$http.post('/api/logout').then((result) => {
        this.user = null
        resolve(result)
      }, (error) => {
        this.user = null
        reject(error)
      })
    })
  }
}

AuthService.$inject = ['$http', '$q']
export default angular.module(
  'folks.services.authservice', []
).service('AuthService', AuthService).name
