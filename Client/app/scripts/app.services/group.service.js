'use strict'
import angular from 'angular'

class GroupService {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.Group = this.$resource(
      'api/group/:groupId',
      {groupId: '@groupId'},
      null,
      {stripTrailingSlashes: false}
    )
  }

  getGroup (groupId) {
    if (groupId) {
      return this.Group.get({groupId: groupId})
    } else {
      return null
    }
  }

  getGroups (criteria) {
    return this.Group.query({criteria: criteria})
  }

  createGroup (groupname, description, creator, participants) {
    return new this.Group({
      groupname: groupname,
      description: description,
      creator: creator,
      participants: participants,
      applicants: []
    }).$save()
  }
}

GroupService.$inject = ['$resource']
export default angular.module(
  'folks.services.groupservice', []
).service('GroupService', GroupService).name
