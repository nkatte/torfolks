'use strict'
import angular from 'angular'

class ToastService {
  constructor (...injected) {
    this.constructor.$inject.forEach((name, index) => {
      this[name] = injected[index]
    })
    this.init()
  }

  init () {
    this.queue = []
    this.nextqueue = []
    this.$rootScope.$on('$routeChangeSuccess', () => {
      this.queue = this.nextqueue
      this.nextqueue = []
    })
  }

  now (message) {
    if (angular.isObject(message)) {
      this.queue = [message]
    } else {
      this.queue = [{message: message, type: 'danger'}]
    }
  }

  next (message) {
    if (angular.isObject(message)) {
      this.nextqueue.push(message)
    } else {
      this.nextqueue.push({message: message, type: 'danger'})
    }
  }

  messages () {
    return this.queue
  }

  close (index) {
    this.queue.splice(index, 1)
  }
}

ToastService.$inject = ['$rootScope']
export default angular.module(
  'folks.services.toastservice', []
).service('ToastService', ToastService).name
