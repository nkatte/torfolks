'use strict'
import gulp from 'gulp'
import source from 'vinyl-source-stream'
import browserify from 'browserify'
import helper from 'babelify-external-helpers'
import proxy from 'http-proxy-middleware'
import del from 'del'

// Include Our Plugins
import eslint from 'gulp-eslint'
import connect from 'gulp-connect'
import inject from 'gulp-inject'

// Clean dist folder
gulp.task('clean', function () {
  return del.sync(['./dist/**', '!./dist'])
})

// Copy fonts files
gulp.task('fonts', function () {
  return gulp.src(['./app/fonts/*'])
    .pipe(gulp.dest('./dist/static/fonts'))
})

// Styles task
gulp.task('styles', function () {
  return gulp.src('./app/styles/*.css')
    .pipe(gulp.dest('./dist/static/css/'))
    .pipe(connect.reload())
})

// Views task
gulp.task('views', function () {
  return gulp.src('./app/views/*')
    .pipe(gulp.dest('./dist/static/partials/'))
    .pipe(connect.reload())
})

// Lint Task
gulp.task('lint', function () {
  return gulp.src('./app/scripts/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
})

// Browserify
gulp.task('js', ['lint'], function () {
  return browserify('./app/scripts/app.js')
  .transform('babelify', {
    plugins: ['external-helpers-2'],
    presets: ['es2015']
  })
  .plugin(helper)
  .bundle()
  .pipe(source('app.js'))
  .pipe(gulp.dest('./dist/static/js'))
  .pipe(connect.reload())
})

// Index task
gulp.task('index', ['styles', 'js'], function () {
  let sources = gulp.src([
    './dist/static/js/*.js',
    './dist/static/css/*.css'
  ], { read: false })
  return gulp.src('./app/index.html')
    .pipe(inject(sources, { addRootSlash: false, ignorePath: 'dist' }))
    .pipe(gulp.dest('./dist/templates'))
    .pipe(connect.reload())
})

gulp.task('server', function () {
  return connect.server({
    middleware: function (connect, opt) {
      return [
        proxy('/api', {
          target: 'http://localhost:8888',
          changeOrigin: true,
          ws: true
        })
      ]
    },
    root: ['./dist'],
    port: 8080,
    livereload: true,
    debug: true,
    fallback: './dist/templates/index.html'
  })
})

gulp.task('watch', function () {
  gulp.watch(['./app/views/*.html'], ['views'])
  gulp.watch(['./app/styles/*.css'], ['styles'])
  gulp.watch(['./app/scripts/*.js', './app/scripts/**/*.js'], ['js'])
  gulp.watch(['./app/index.html'], ['index'])
})

// Build the pack
gulp.task('build', [
  'views',
  'fonts',
  'index'
])

// Default Task
gulp.task('default', [
  'build',
  'server',
  'watch'
])
