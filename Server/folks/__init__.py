import tornado
from folks.url import url_patterns
from folks.config import settings

app = tornado.web.Application(url_patterns, **settings)

__version__ = '0.0.1'
