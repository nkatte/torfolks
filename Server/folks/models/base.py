from bson import json_util
from motorengine import (Document, Q)


class BaseModel(Document):

    def __eq__(self, obj):
        if hasattr(obj, '_id'):
            return self._id == obj._id
        return False

    def to_json(self):
        raise NotImplementedError

    @classmethod
    def from_json(self, obj):
        raise NotImplementedError

    @classmethod
    def create_query(cls, query):
        q = Q()
        if query:

            def build(query):
                if query['field'] in cls._fields:
                    return Q(**{query['field']: query['data']})
                return Q()

            if not isinstance(query, list):
                query = [query]
            for _ in query:
                q = q | build(_)
        return q
