from bson import ObjectId
from motorengine import (
    ReferenceField,
    StringField,
    ListField
)

from folks.models.base import BaseModel
from folks.models.group import Group
from folks.models.user import User


class Message(BaseModel):
    __lazy__ = False
    user = ReferenceField(User, required=True)
    group = ReferenceField(Group, required=True)
    message = StringField(required=True)
    tags = ListField(StringField(), required=True, default=['#todas'])

    def to_json(self):
        return {
            'user': self.user.to_json(),
            'group': self.group.to_json(),
            'message': self.message,
            'tags': self.tags
        }

    @classmethod
    def from_json(cls, obj):
        message = cls(
            user=ObjectId(obj['user']),
            group=ObjectId(obj['group']),
            message=obj['message']
        )
        if obj['tags']:
            message.tags += obj['tags']
            TODO(nkatte): Limpa as tags
            obj['tags'] = []
        return message
