from bson import ObjectId
from motorengine import (StringField, ReferenceField, ListField)

from folks.models.base import BaseModel
from folks.models.user import User


class Group(BaseModel):
    __lazy__ = False
    groupname = StringField(required=True, unique=True)
    description = StringField(required=True)
    creator = ReferenceField(User, required=True)
    participants = ListField(ReferenceField(User), required=True)
    applicants = ListField(ReferenceField(User))

    def to_json(self):
        return {
            'id': str(self._id),
            'groupname': self.groupname,
            'description': self.description,
            'creator': self.creator.to_json(),
            'participants': [user.to_json() for user in self.participants],
            'applicants': [user.to_json() for user in self.applicants]
        }

    @classmethod
    def from_json(cls, obj):
        group = cls(
            groupname=obj['groupname'],
            description=obj['description'],
            creator=ObjectId(obj['creator']['id']),
            participants=[
                ObjectId(user['id']) for user in obj['participants']
            ],
            applicants=[
                ObjectId(user['id']) for user in obj['applicants']
            ]
        )
        if 'id' in obj:
            group._id = ObjectId(obj['id'])
        return group
