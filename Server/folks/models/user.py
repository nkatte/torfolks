from bson import ObjectId
from motorengine import StringField
from passlib.hash import sha256_crypt

from folks.models.base import BaseModel


class User(BaseModel):
    __lazy__ = False
    username = StringField(required=True, unique=True)
    fullname = StringField(required=True)
    password = StringField(required=True)
    image = StringField(required=True)

    def encrypt_password(self, password):
        self.password = sha256_crypt.encrypt(password)

    def authenticate(self, password):
        return sha256_crypt.verify(password, self.password)

    def to_json(self):
        return {
            'id': str(self._id),
            'username': self.username,
            'fullname': self.fullname,
            'image': self.image
        }

    @classmethod
    def from_json(cls, obj):
        user = cls(
            username=obj['username'],
            fullname=obj['fullname'],
            image=obj['image']
        )
        if 'id' in obj:
            user._id = ObjectId(obj['id'])
        if 'password' in obj:
            user.password = obj['password']
        return user
