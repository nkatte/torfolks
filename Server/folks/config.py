import os
from concurrent.futures import ThreadPoolExecutor

from motorengine import connect
from tornado.process import cpu_count

ROOT = os.path.dirname(os.path.abspath(__file__))
MEDIA_ROOT = os.path.join(ROOT, 'public', 'static')
TEMPLATE_ROOT = os.path.join(ROOT, 'public', 'templates')
DEBUG = os.getenv('DEBUG', False)
DB = os.getenv('DATABASE_URI', 'mongodb://127.0.0.1:27017')

settings = dict()
settings['debug'] = DEBUG
settings['db'] = connect(db="folks", host=DB)
settings['static_path'] = MEDIA_ROOT
settings['template_path'] = TEMPLATE_ROOT
settings['cookie_secret'] = os.urandom(64)
settings['xsrf_cookies'] = True
settings['executor'] = ThreadPoolExecutor(cpu_count() * 2 + 1)
