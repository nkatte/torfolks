import tornado.escape
import tornado.gen

from folks.handlers.base import ApiHandler
from folks.models.user import User


class LoginHandler(ApiHandler):

    @tornado.gen.coroutine
    def post(self):
        username = self.json_args.get("username", "")
        password = self.json_args.get("password", "")
        user = yield User.objects.get(username=username)
        if user:
            auth = yield self.executor.submit(user.authenticate, password)
            if auth:
                self.set_secure_cookie(self.USER_COOKIE, str(user._id))
                return self.finish_json({'user': user.to_json()})
        self.send_error(403, reason="The username or password are incorrect.")


class LogoutHandler(ApiHandler):
    @tornado.gen.coroutine
    def post(self):
        self.clear_cookie(self.USER_COOKIE)
