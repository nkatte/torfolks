import tornado.gen
import tornado.web
from motorengine.errors import (UniqueKeyViolationError, InvalidDocumentError)

from folks.handlers.base import ApiHandler
from folks.models.user import User


class UserHandler(ApiHandler):

    @tornado.gen.coroutine
    @tornado.web.authenticated
    def get(self, user_id):
        if user_id:
            user = yield User.objects.get(id=user_id)
            if user:
                return self.finish_json(user.to_json())
            return self.send_error(status_code=404, reason="User not found.")
        query = User.create_query(self.criteria)
        users = yield User.objects.filter(query).find_all()
        self.finish_json([user.to_json() for user in users])

    @tornado.gen.coroutine
    def post(self, user_id):
        try:
            user = User.from_json(self.json_args)
            yield self.executor.submit(user.encrypt_password, user.password)
            yield user.save()
        except InvalidDocumentError:
            return self.send_error(400, reason="The user is invalid.")
        except UniqueKeyViolationError:
            return self.send_error(403, reason="This user already exists.")
        self.finish_json(user.to_json())
