import json
import tornado.gen
import tornado.web
import tornado.websocket
from bson import json_util

from folks.models.user import User


class BaseHandler(tornado.web.RequestHandler):
    USER_COOKIE = "userId"

    def initialize(self):
        self.xsrf_token

    @tornado.gen.coroutine
    def prepare(self):
        user_id = self.get_secure_cookie(self.USER_COOKIE, None)
        if user_id:
            self.current_user = yield User.objects.get(
                id=user_id.decode('utf-8')
            )

    @property
    def executor(self):
        return self.settings.get('executor', None)


class ApiHandler(BaseHandler):

    def initialize(self):
        self.json_args = {}
        if self.request.headers.get("Content-Type", "").startswith(
                "application/json"):
            self.json_args = json.loads(self.request.body.decode('utf-8'))
        if self.request.method == 'GET':
            self.skip = self.get_argument('skip', None)
            self.limit = self.get_argument('limit', None)
            self.criteria = json_util.loads(
                self.get_argument('criteria', 'null'),
                parse_constant=True
            )

    def get_login_url(self):
        raise tornado.web.HTTPError(403, reason="User not authenticated")

    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")

    def write_json(self, chunk):
        self.write(json.dumps(chunk))

    def finish_json(self, chunk):
        self.finish(json.dumps(chunk))

    def write_error(self, status_code=500, **kwargs):
        reason = kwargs.get('reason', None)
        self.finish_json({
            "status": status_code,
            "reason": reason
        })


class WebSocketBaseHandler(BaseHandler, tornado.websocket.WebSocketHandler):
    pass
