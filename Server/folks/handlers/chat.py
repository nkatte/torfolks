from json import loads, dumps
from bson import ObjectId
import motorengine.errors
import tornado.gen
import tornado.web

from folks.handlers.base import WebSocketBaseHandler
from folks.models.group import Group
from folks.models.message import Message


class ChatHandler(WebSocketBaseHandler):
    chats = {}

    @tornado.gen.coroutine
    @tornado.web.authenticated
    def open(self, group_id):
        if not group_id:
            return self.close(400, "You have to specify a group id.")
        group = yield Group.objects.get(id=group_id)
        if not group:
            return self.close(404, "Group not found.")
        if self.current_user not in group.participants:
            return self.close(401, "You do not belong to this group")
        self.group_id = group_id
        ChatHandler.chats.setdefault(group_id, set()).add(self)
        messages = yield \
            Message.objects.filter(group=group).limit(70).find_all()
        for message in messages:
            self.write_message(
                dumps({'event': '$message', 'data': message.to_json()})
            )

    @tornado.gen.coroutine
    def on_message(self, data):
        event = loads(data)
        if event['event'] == '$message':
            message = Message.from_json(event['data'])
            try:
                message.validate()
            except motorengine.errors.InvalidDocumentError:
                return self.close(400, "Poorly formatted data.")
            yield message.save()
            yield message.load_references()
            self.broadcast('$message', [message])
        elif event['event'] == '$load':
            query = Message.create_query(event['data'])
            # TODO(nkatte): limit hardcoded
            messages = yield Message.objects.filter(
                    group=ObjectId(self.group_id)
                ).filter(query).limit(70).find_all()
            for message in messages:
                self.write_message(
                    dumps({'event': '$message', 'data': message.to_json()})
                )
        else:
            return self.close(400, "Poorly formatted data.")

    def on_close(self):
        if self in ChatHandler.chats.get(self.group_id, {}):
            ChatHandler.chats[self.group_id].remove(self)
        if len(ChatHandler.chats) == 0:
            ChatHandler.chats.pop(self.group_id)

    def broadcast(self, event, messages):
        for client in ChatHandler.chats[self.group_id]:
            for message in messages:
                client.write_message(
                    dumps({'event': event, 'data': message.to_json()})
                )

    def check_origin(self, origin):
        return True
