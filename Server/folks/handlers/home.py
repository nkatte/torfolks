from folks.handlers.base import BaseHandler
import tornado.gen


class HomeHandler(BaseHandler):

    @tornado.gen.coroutine
    def get(self, *args):
        self.render('index.html')
