import tornado.gen
import tornado.web
from motorengine.errors import (InvalidDocumentError, UniqueKeyViolationError)

from folks.handlers.base import ApiHandler
from folks.models.group import Group


class GroupHandler(ApiHandler):

    @tornado.gen.coroutine
    @tornado.web.authenticated
    def get(self, group_id):
        if group_id:
            group = yield Group.objects.get(id=group_id)
            if group:
                return self.finish_json(group.to_json())
            return self.send_error(
                400,
                reason="The group id specified is invalid."
            )
        query = Group.create_query(self.criteria)
        groups = yield Group.objects.filter(query).find_all()
        self.finish_json([group.to_json() for group in groups])

    @tornado.gen.coroutine
    @tornado.web.authenticated
    def post(self, group_id):
        try:
            group = Group.from_json(self.json_args)
            yield group.save()
            yield group.load_references()
        except InvalidDocumentError:
            return self.send_error(400, reason="Poorly formatted data.")
        except UniqueKeyViolationError:
            return self.send_error(403, reason="This group already exists.")
        self.finish_json(group.to_json())
