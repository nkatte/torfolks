from folks.handlers import (home, auth, user, group, chat)

url_patterns = [
    (r"/api/login", auth.LoginHandler),
    (r"/api/logout", auth.LogoutHandler),
    (r"/api/user/(?P<user_id>.*)", user.UserHandler),
    (r"/api/group/(?P<group_id>.*)", group.GroupHandler),
    (r"/api/chat/(?P<group_id>.*)", chat.ChatHandler),
    (r"/(.*)", home.HomeHandler)
]
