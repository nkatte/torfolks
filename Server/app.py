#!/usr/bin/env python3

import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.log import app_log
from tornado.options import options, define

from folks import app

define("host", default="localhost", help="address to your server")
define("port", default=8888, help="run on the given port", type=int)


def main():
    options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(address=options['host'], port=options['port'])
    try:
        app_log.info(
            "Starting server at {0}:{1}"
            .format(options['host'], options['port'])
        )
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        app_log.info("Stopping server")


if __name__ == "__main__":
    main()
